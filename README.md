# Dev Hops

A collection of notes and snippets for the aspiring DevOps practitioner.

## vps/

### vps/SETUP

The `SETUP.md` file contains a guide to securing access to Debian and to Plesk, when available. Only secure access is covered, other security aspects as well as usability improvements or other relevant configuration options are covered in separate guides.

### vps/DEBIAN

The `DEBIAN.md` file contains suggestions concerning useful applications to install at the operating system level, as well as certain configuration setups to improve usability via SSH. The content is written with Debian in mind, but most suggestions will apply to any Linux operating system.

### vps/PLESK

The `PLESK.md` file contains a comprehensive setup for the Plesk web admin panel, in conjunction with some DNS work which might take place at the registrar level.
