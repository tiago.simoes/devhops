# Plesk Tips & Tricks

Plesk is a great web admin panel but it has some quirks and limitations that require some attention to details.

## Branding the server

Accessing Plesk via the IPv4 address or the original URL is discouraged because of the security warnings related to the self-signed certificate. As such, a bit of branding is in order.

Under _General Settings_ > _Server Settings_ change the hostname to something like `vps.domain.tld` and tweak the rest of the options at will.

Under _General Settings_ > _Customize Plesk URL_ select a designated URL and use `vps.domain.tld` as well.

Under _Security_ > _SSL/TLS Certificates_ generate a _Let's Encrypt_ certificate and assign it to Plesk.

The assignment of the new certificate will trigger a security alert from the browser, as expected. Log out, go to the new URL and log back into Plesk.

**NOTE:** the above may not work immediately; it may be necessary to create a proper webspace and have the server listen for that exact URL for the redirect to the Plesk login path to work properly.

Under _Plesk Appearance_ > _Branding_ change the page titles for something more appropriate. 

## Use safe passwords

Under _Security_ > _Security Policy_ set the _Password Strength_ to _Very strong_ - this will affect every password created in Plesk from that point on.

## Trust the system updates

Under _System Settings_ > _System Updates_ > _Settings_ leave everything selected as recommended, but switch off email notifications.
