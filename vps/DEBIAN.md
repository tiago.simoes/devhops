# Debian Tips & Tricks

Debian is a rock solid system, thus somewhat conservative. With small tweaks it becomes fairly easy to use in daily life.

## Useful applications

While the fundamentals like `git` will likely be installed in the system, some other basic programs can make the CLI experience much more practical and enjoyable.

Log in as the admin user `debian` and install a couple of CLI utilities for added comfort:

```
sudo apt update && sudo apt install htop tree
```

## Useful tweaks

The system should ship with the `bash` shell and the admin user should have a `.bashrc` already in place. In this repo, under the `dotfiles` directory, there are two copies of that `.bashrc` file where the only changes are the prompt colors - to assist in distinguishing between the user's own machine, the admin user on the server and any other user on the server.

Furthermore, there is a useful collection of `.bash_aliases` that can be dropped into any `$HOME` directory in the system.

In order to enable `.bashrc` and `.bash_aliases` for every user, it's necessary to add a `.bash_profile` to each `$HOME` directory, so that the right files get sourced upon login.

Regarding login, the above will take effect whenever a certain user logs in via `ssh` or when the admin user `debian` logs into a user account directly via the following command:

```
sudo su ${USER} --login
```

### The `$PATH` is important

There is a larger discussion regarding `.bash_profile` which is more closely connected to the concept of "webspace" in a system running Plesk - refer to `WEBSPACE.md` for more details.
