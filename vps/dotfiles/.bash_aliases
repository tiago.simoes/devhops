# Extended core commands
alias mkdir="mkdir -p"

# List and Tree
alias ls="ls --color=auto --group-directories-first"
alias ll="ls -hAl"

alias lt="tree -L 3 -d -C | less -r"
alias tt="tree -L 2 -C | less -r"
alias ttt="tree -L 3 -C | less -r"
