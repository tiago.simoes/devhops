# Read user's .bashrc if present
if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi

# To use the PHP binaries provided by Plesk,
# uncomment one of the following lines:

#PATH=/opt/plesk/php/8.1/bin:$PATH:$HOME/bin
PATH=/opt/plesk/php/8.0/bin:$PATH:$HOME/bin

# Increase the memory limit for PHP CLI
alias php="php -d memory_limit=512M"
