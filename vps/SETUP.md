# Setting up a VPS running Debian

This _very opinionated guide_ covers the basic setup of a Debian system in a provisioned VPS.

## What the provider provides

The VPS provider will provide the following:

  - the IPv4 address of the VPS
  - the hostname of the VPS
  - the admin username, which will be `debian`
  - the admin password, which must be changed!

If the *Plesk* web admin panel is installed, those credentials will be provided as well. See below for more information.

## Securing access to the operating system

In order to secure access to the VPS via SSH a few initial steps are in order:

1. Log into the VPS as the admin user: `ssh debian@hostname`
2. Set a new password for the admin user: `passwd`
3. Switch to the root user of the system: `sudo su`
4. Set a new password for the root user: `passwd`
5. Switch back to the admin user: `exit`
6. Log out from the VPS: `exit`
7. Confirm that `ssh root@hostname` doesn't work at all
8. Confirm that `ssh debian@hostname` works with the new password
9. Confirm that `sudo su` still switches to the root user
10. **Never be root. _EVER._**

If it all went well, great. If not, time to reinstall the VPS. Oops.

## Securing access to the web admin panel

In order to secure access to the VPS via Plesk some more steps are in order:

1. Go to http://hostname:8443 and _accept the risk_ of the self signed certificate
2. Log into Plesk as `root` with the new password from the previous checklist (4)
3. Configure the Plesk admin account and activate Plesk
4. Go to _Extensions_ and install _Panel.ini Editor_
5. Open the _Panel.ini_ in _Editor_ mode and add the following lines at the end:

```
[login]
systemAdmin = false
```

6. Log out of Plesk
7. Confirm that `root` login is blocked
8. Confirm that regular login works normally
